# core/serializar.py

# Importaciones de Django
from djoser.serializers import UserCreateSerializer, UserSerializer
# Importaciones de REST
from rest_framework import serializers

# Importaciones de otras app del proyecto
from . import models

class UserRegistrationSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        fields = ('id', 'document_type', 'document', 'username', 'first_name', 'last_name', 'photo', 'gender', 'birth_date',
        'password', 'email', 'is_athlete', 'is_coach')

class UserMeSerializer(UserSerializer):
    class Meta(UserSerializer.Meta):
        fields = ('id', 'document_type', 'document', 'username', 'first_name', 'last_name', 'photo', 'gender', 'birth_date',
        'password', 'email', 'is_athlete', 'is_coach')

class User_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('id', 'document_type', 'document', 'username', 'first_name', 'last_name', 'photo', 'gender', 'birth_date', 'password', 'email', 'is_athlete', 'is_coach')
          # 'is_staff', 'is_active', 'date_joined', 'groups', 'user_permissions', 'last_login', 'is_superuser')

class Residence_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Residence
        fields = '__all__'

class Training_Day_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Training_Day
        fields = '__all__'

class Group_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = '__all__'

class Training_Group_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Training_Group
        fields = '__all__'

class Parent_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Parent
        fields = '__all__'

class Athlete_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Athlete
        fields = '__all__'

class Payment_Serializer(serializers.ModelSerializer):
    class Meta:
        model = models.Payment
        fields = '__all__'



class User_Profile_Serializer(serializers.ModelSerializer):
    user=serializers.StringRelatedField(read_only=True)
    class Meta:
        model=models.User
        fields='__all__'