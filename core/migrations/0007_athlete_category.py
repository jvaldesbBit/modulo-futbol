# Generated by Django 2.2.9 on 2020-06-01 01:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0002_concept_payment_method'),
        ('core', '0006_auto_20200529_0323'),
    ]

    operations = [
        migrations.AddField(
            model_name='athlete',
            name='category',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, to='data.Category', verbose_name='Categoria'),
            preserve_default=False,
        ),
    ]
