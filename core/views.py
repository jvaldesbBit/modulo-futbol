# core/views.py

# Importaciones de Django
from django.shortcuts import render
import datetime
from .permissions import IsOwnerProfileOrReadOnly, IsCoachUser, IsAdministrativeUser

# Importaciones de REST
from rest_framework.generics import (ListCreateAPIView,RetrieveUpdateDestroyAPIView,)
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, generics, response, views
from django.views import View
from django.http import HttpResponse

# Importaciones de otras app del proyecto
from . import models
from . import serializer
from . import report

class User_Profile_List_Create_View(ListCreateAPIView):
    queryset=models.User.objects.all()
    serializer_class=serializer.User_Profile_Serializer
    permission_classes=[IsAuthenticated]

    def perform_create(self, serializer):
        user=self.request.user
        serializer.save(user=user)


class User_Profile_Detail_View(RetrieveUpdateDestroyAPIView):
    queryset=models.User.objects.all()
    serializer_class=serializer.User_Profile_Serializer
    permission_classes=[IsOwnerProfileOrReadOnly,IsAuthenticated]


class User_Viewset(viewsets.ModelViewSet):
    serializer_class = serializer.User_Serializer
    #permission_classes = [IsAuthenticated]
    http_method_names = ['get', 'put', 'patch', 'head', 'options', 'delete']
    queryset = models.User.objects.all()
"""
    def get_queryset(self):
        print(self)
        user = self.request.user.id
        return response.Response(models.User.objects.filter(id = user))"""

class Residence_Viewset(viewsets.ModelViewSet):
    queryset = models.Residence.objects.all()
    serializer_class = serializer.Residence_Serializer
    #permission_classes = [IsAuthenticated]

class Training_Day_Viewset(viewsets.ModelViewSet):
    queryset = models.Training_Day.objects.all()
    serializer_class = serializer.Training_Day_Serializer
    #permission_classes = [IsAuthenticated]

class Group_Viewset(viewsets.ModelViewSet):
    queryset = models.Group.objects.all()
    serializer_class = serializer.Group_Serializer
    #permission_classes = [IsAuthenticated]

class Training_Group_Viewset(viewsets.ModelViewSet):
    queryset = models.Training_Group.objects.all()
    serializer_class = serializer.Training_Group_Serializer
    #permission_classes = [IsAuthenticated]

class Parent_Viewset(viewsets.ModelViewSet):
    queryset = models.Parent.objects.all()
    serializer_class = serializer.Parent_Serializer
    #permission_classes = [IsAuthenticated]

class Athlete_Viewset (viewsets.ModelViewSet):
    queryset = models.Athlete.objects.all()
    serializer_class = serializer.Athlete_Serializer

class Payment_Viewset(viewsets.ModelViewSet):
    queryset = models.Payment.objects.all()
    serializer_class = serializer.Payment_Serializer
    http_method_names = ['post', 'get', 'head', 'options']

class Athlete_Full_Details(views.APIView):
    #permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            id = request.query_params['athlete_id']
            athlete = models.Athlete.objects.get(id=id)
            days = []
            day_dic = {'MO': 'Lunes', 'TU': 'Martes', 'WE': 'Miercoles', 'TH': 'Jueves', 'FR':'Viernes', 'SA':'Sabado', 'SU': 'Domingo'}
            for day in models.Training_Group.objects.all().filter(group=athlete.training_schedule.id):
                days.append(day_dic[day.training_day.day])

            respon = {
                "id": athlete.id.id,
                "document_type": athlete.id.document_type.document,
                "document": athlete.id.document,
                "username": athlete.id.username,
                "first_name": athlete.id.first_name,
                "last_name": athlete.id.last_name,
                "gender": athlete.id.gender,
                "birth_date": athlete.id.birth_date,
                "email": athlete.id.email,
                "birth_department": athlete.birth_department.department,
                "birth_municipality": athlete.birth_municipality.municipality,
                "residence": athlete.residence.residence_address,
                "training_schedule": days,
                "category": athlete.category.category
            }

        except Exception as e:
            return response.Response({'status': 'No procesado'}, 202)

        return response.Response(respon, 200)



class Get_Receipt_Payment_PDF (views.APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        try:
            document = request.data.get('athlete')
            user = models.User.objects.get(document=document)
            athlete = models.Athlete.objects.get(id=user.id)


        except Exception as e:
            return response.Response({'status': 'No procesado'}, 202)

        pdf = report.render_pdf('core/payment.html', {'id': 0})
        response_pdf = HttpResponse(pdf, content_type='application/pdf')

        return response_pdf


class Get_Pay(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            document = request.data.get('athlete')
            user = models.User.objects.get(document=document)
            athlete = models.Athlete.objects.get(id=user.id)
            value = athlete.training_schedule.monthly_value
            tomorrow = datetime.datetime.now() + datetime.timedelta(days=15)
            models.Payment.objects.create()
        except Exception as e:
            return response.Response({'status': 'No procesado'}, 202)


