from rest_framework.permissions import BasePermission,SAFE_METHODS

class IsOwnerProfileOrReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.user==request.user

class IsCoachUser(BasePermission):
    """
    Allows access only to coach users.
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_coach)

class IsAdministrativeUser(BasePermission):
    """
    Allows access only to administrative users.
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_administrative)