from django.contrib import admin

from .models import User, Residence, Training_Day, Group, Training_Group, Parent, Athlete, Payment 

admin.site.register(User)
admin.site.register(Residence)
admin.site.register(Training_Day)
admin.site.register(Group)
admin.site.register(Training_Group)
admin.site.register(Parent)
admin.site.register(Athlete)
admin.site.register(Payment)
