from django.template.loader import get_template
import json
from weasyprint import HTML, CSS

def render_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    pdf_file = HTML(string=template).write_pdf()
    return pdf_file