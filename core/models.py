# core/models.py

# Importaciones de Django
from django.contrib.auth.models import AbstractUser
from django.db import models
from computedfields.models import ComputedFieldsModel, computed

# Importaciones desde otras App del proyecto
from data.models import Document_Type, Municipality, Department, Category, Payment_Method, Concept
from datetime import date, datetime, timedelta


class User(AbstractUser):
    MALE = 'M'
    FEMALE = 'F'
    GENDER_CHOICES = [
        (MALE, 'Masculino'),
        (FEMALE, 'Femenino'),
    ]
    document = models.CharField(max_length=12, null=True, unique=True, verbose_name='Documento de identificación')
    document_type = models.ForeignKey(Document_Type, null=True, verbose_name='Tipo de documento',
                                      on_delete=models.CASCADE)
    photo = models.ImageField(null=True, upload_to='users', height_field=None, width_field=None, max_length=None,
                              verbose_name='Foto', blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name='Genero')
    birth_date = models.DateField(auto_now=False, null=True, auto_now_add=False, verbose_name='Fecha de nacimiento')
    is_athlete = models.BooleanField(default=False, verbose_name='Es atleta')
    is_coach = models.BooleanField(default=False, verbose_name='Es entrenador')
    is_administrative = models.BooleanField(default=False, verbose_name='Es usuario administrativo')

    class Meta:
        db_table = 'auth_user'
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'


class Residence(models.Model):
    residence_address = models.TextField(verbose_name="Dirección de residencia", max_length=200)
    department = models.ForeignKey(Department, verbose_name="Departamento", on_delete=models.CASCADE)
    municipality = models.ForeignKey(Municipality, verbose_name="Municipio", on_delete=models.CASCADE)
    neighborhood = models.CharField(verbose_name="Barrio", max_length=50)
    phone_line = models.CharField(verbose_name="Número de telefono", max_length=12)

    class Meta:
        verbose_name = 'Residencia'
        verbose_name_plural = 'Residencias'

    def __str__(self):
        return str(self.id) + " " + self.residence_address


class Training_Day(models.Model):
    MONDAY = "MO"
    TUESDAY = "TU"
    WEDNESDAY = "WE"
    THURSDAY = "TH"
    FRIDAY = "FR"
    SATURDAY = "SA"
    SUNDAY = "SU"
    DAYS_WEEK_CHOICES = [
        (MONDAY, "Lunes"),
        (TUESDAY, "Martes"),
        (WEDNESDAY, "Miércoles"),
        (THURSDAY, "Jueves"),
        (FRIDAY, "Viernes"),
        (SATURDAY, "Sábado"),
        (SUNDAY, "Domingo"),
    ]
    training = models.CharField(verbose_name="Nombre dia de entrenamiento", max_length=200)
    day = models.CharField(max_length=2, choices=DAYS_WEEK_CHOICES, verbose_name='Dia')
    coach = models.ForeignKey(User, verbose_name="Entrenador", on_delete=models.CASCADE,
                              limit_choices_to={'is_coach': True})

    class Meta:
        verbose_name = 'Dias de entrenamiento'
        verbose_name_plural = 'Dias de entrenamiento'

    def __str__(self):
        return str(self.id) + " " + self.training


class Group(models.Model):
    group = models.CharField(verbose_name="Nombre grupo de entrenamiento", max_length=200)
    monthly_value = models.FloatField(verbose_name="Valor mensualidad")
    category = models.ForeignKey(Category, verbose_name="Categoria", on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Grupo de entrenamiento'
        verbose_name_plural = 'Grupos de entrenamiento'

    def __str__(self):
        return str(self.id) + " " + self.group


class Training_Group(models.Model):
    group = models.ForeignKey(Group, verbose_name="Grupo de entrenamiento", on_delete=models.CASCADE)
    training_day = models.ForeignKey(Training_Day, verbose_name="Grupo de entrenamiento", on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Grupo y dia'
        verbose_name_plural = 'Grupos y dias'

    def __str__(self):
        return str(self.id)


class Parent(models.Model):
    document_type = models.ForeignKey(Document_Type, null=True, verbose_name='Tipo de documento',
                                      on_delete=models.CASCADE)
    document = models.CharField(max_length=12, null=True, unique=True, verbose_name='Documento de identificación')
    first_name = models.CharField(verbose_name="Nombre", max_length=200)
    last_name = models.CharField(verbose_name="Apellidos", max_length=200)
    cell_phone = models.CharField(verbose_name="Número de celular", max_length=12)

    class Meta:
        verbose_name = 'Padre o tutor'
        verbose_name_plural = 'Padres o tutores'

    def __str__(self):
        return str(self.id)


class Athlete(ComputedFieldsModel):
    id = models.OneToOneField(User, verbose_name="Id del usuario", on_delete=models.CASCADE, primary_key=True)
    birth_department = models.ForeignKey(Department, verbose_name="Departamento de nacimiento",
                                         on_delete=models.CASCADE)
    birth_municipality = models.ForeignKey(Municipality, verbose_name="Municipio de nacimiento",
                                           on_delete=models.CASCADE)
    residence = models.ForeignKey(Residence, verbose_name="Residencia", on_delete=models.CASCADE)
    # training_schedule = models.ForeignKey(Group, verbose_name="Grupo de entrenamiento", on_delete=models.CASCADE)
    # category = models.ForeignKey(Category, verbose_name="Categoria", on_delete=models.CASCADE, blank=True)
    parent = models.ForeignKey(Parent, verbose_name="Tutor", on_delete=models.CASCADE)

    @computed(models.ForeignKey(Category, verbose_name="Categoria", on_delete=models.CASCADE, blank=True),
              depends=[['self', ['id']]])
    def category(self):
        birth_date = self.id.birth_date
        now = date.today()
        age = now.year - birth_date.year - ((now.month, now.day) < (birth_date.month, birth_date.day))
        print(age)
        return Category.objects.get(start_age=age)

    @computed(models.ForeignKey(Group, verbose_name="Grupo de entrenamiento", on_delete=models.CASCADE, blank=True),
              depends=[['self', ['category']]])
    def training_schedule(self):
        return Group.objects.get(category=self.category.id)

    class Meta:
        verbose_name = 'Atleta'
        verbose_name_plural = 'Atletas'

    def __str__(self):
        return str(self.id.id) + " " + str(self.id.document) + " " + self.id.last_name + " " + self.id.first_name


class Payment(ComputedFieldsModel):
    payment_method = models.ForeignKey(Payment_Method, verbose_name="Metodo de pago", on_delete=models.CASCADE)
    concept = models.ForeignKey(Concept, verbose_name="Concepto", on_delete=models.CASCADE)
    athlete = models.ForeignKey(Athlete, verbose_name="Atleta", on_delete=models.CASCADE)
    # value = models.IntegerField(verbose_name='Valor a pagar')
    # expedition_date = models.DateField(auto_now=False, null=True, auto_now_add=False, verbose_name='Fecha de expedición')
    # payment_date = models.DateField(auto_now=False, null=True, auto_now_add=False, verbose_name='Fecha de pago oportuno')
    file = models.ImageField(null=True, upload_to='payment', height_field=None, width_field=None, max_length=None,
                             verbose_name='Fichero', blank=True)
    payment_made = models.BooleanField(verbose_name="Pago realizado", blank=True)
    status_payment = [("pa", "Pago aprobado"), ("pr", "Pago rechazado"), ("ap", "Abono al pago"), ("sf", "Saldo faltante")]
    status = models.CharField(verbose_name="Estado del pago", choices=status_payment, max_length=3)
    partial_payment = models.IntegerField(verbose_name='Pago parcial', blank=True, null=True)

    @computed(models.IntegerField(verbose_name='Valor a pagar', blank=True, null=True), depends=[['self', ['athlete']]])
    def value(self):
        return self.athlete.training_schedule.monthly_value

    @computed(
        models.DateField(auto_now=False, null=True, auto_now_add=False, verbose_name='Fecha de expedición', blank=True),
        depends=[['self', ['athlete']]])
    def expedition_date(self):
        return datetime.now().date()

    @computed(models.DateField(auto_now=False, null=True, auto_now_add=False, verbose_name='Fecha de pago oportuno',
                               blank=True),
              depends=[['self', ['athlete']]])
    def payment_date(self):
        date_pay = datetime.now() + timedelta(days=15)
        return date_pay.date()

    @computed(models.IntegerField(verbose_name='Saldo', blank=True, null=True),
              depends=[['self', ['athlete', 'partial_payment', 'value']]])
    def balance(self):
        return self.value - self.partial_payment

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'

    def __str__(self):
        return str(self.id)
