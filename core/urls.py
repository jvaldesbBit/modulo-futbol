from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'user', views.User_Viewset, basename='user')
router.register(r'recidence', views.Residence_Viewset)
router.register(r'training_day', views.Training_Day_Viewset)
router.register(r'group', views.Group_Viewset)
router.register(r'training_group', views.Training_Group_Viewset)
router.register(r'parent', views.Parent_Viewset)
router.register(r'athlete', views.Athlete_Viewset)
router.register(r'payment', views.Payment_Viewset)
#router.register(r'all-profiles', viewsUser.User_Profile_List_Create_View)

urlpatterns = [
    path("get_payment", views.Get_Receipt_Payment_PDF.as_view(), name="get_payment"),
    path("athlete_full_details", views.Athlete_Full_Details.as_view(), name="athlete_full_details"),
    path("", include(router.urls)),
    path("all-profiles", views.User_Profile_List_Create_View.as_view(), name="all-profiles"),
]
