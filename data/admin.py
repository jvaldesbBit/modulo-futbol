from django.contrib import admin

from .models import Document_Type, Payment_Method, Concept, Department, Municipality, Category

admin.site.register(Document_Type)
admin.site.register(Payment_Method)
admin.site.register(Concept)
admin.site.register(Department)
admin.site.register(Municipality)
admin.site.register(Category)
