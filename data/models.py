# data/models.py

# Importaciones de Django
from django.db import models


class Document_Type(models.Model):
    document = models.CharField(verbose_name='Tipo de documento', max_length=60)
    acronym = models.CharField(verbose_name='Siglas', max_length=3)
    description = models.TextField(verbose_name='Descripción', max_length=250)

    class Meta:
        verbose_name = 'Tipo de documento'
        verbose_name_plural = 'Tipos de documentos'

    def __str__(self):
        return str(self.id) + " " + self.document

class Payment_Method(models.Model):
    payment_method = models.CharField(verbose_name='Metodo de pago', max_length=60)
    acronym = models.CharField(verbose_name='Siglas', max_length=3)
    description = models.TextField(verbose_name='Descripción', max_length=250)

    class Meta:
        verbose_name = 'Metodo de pago'
        verbose_name_plural = 'Metodos de pago'

    def __str__(self):
        return str(self.id) + " " + self.payment_method


class Concept(models.Model):
    concept = models.CharField(verbose_name='Concepto', max_length=60)
    acronym = models.CharField(verbose_name='Siglas', max_length=3)
    description = models.TextField(verbose_name='Descripción', max_length=250)

    class Meta:
        verbose_name = 'Concepto'
        verbose_name_plural = 'Conceptos'

    def __str__(self):
        return str(self.id) + " " + self.concept


class Department(models.Model):
    department = models.CharField(verbose_name='Departamento', max_length=200)
    region = models.TextField(verbose_name='Región', max_length=500)

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'

    def __str__(self):
        return str(self.id) + " " + self.department


class Municipality(models.Model):
    municipality = models.CharField(verbose_name='Municipio', max_length=200)
    department = models.ForeignKey(Department, null=True, verbose_name='Departamento', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'

    def __str__(self):
        return str(self.id) + " " + self.municipality

class Category(models.Model):
    category = models.CharField(verbose_name='Categoria', max_length=200)
    start_age = models.IntegerField(verbose_name='Edad inicial')
    end_age = models.IntegerField(verbose_name='Edad final')
    acronym = models.CharField(verbose_name='Siglas', max_length=3)
    description = models.TextField(verbose_name='Descripción', max_length=250)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return str(self.id) + " " + self.category
